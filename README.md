# How to use
* Create a Google Cloud Platform project
* Create a Cloud SQL instance ("wordpress" is a reasonable name)
  * gcloud sql instances create wordpress
* Create a new database within the instance named "wordpress\_db"
* Enable App Engine in the GCP project
* Edit wordpress/wp-config.php
  * Change gae\_cloudsql\_instance to the Cloud SQL instance you created above
* Deploy to App Engine
  * gcloud app deploy
* Go to <gcp-project>.appspot.com/wp-admin/ and complete the WordPress setup
* TODO: describe in-Wordpress GAE setup
